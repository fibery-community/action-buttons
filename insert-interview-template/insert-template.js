const fibery = context.getService('fibery');

const TYPE_NAME = "Interview";
const RICH_TEXT_FIELD_NAME = "Notes";
const PARENT_TYPE_NAME = "Study";
const TEMPLATE_FIELD_NAME = "Interview Template";


const interviews = args.currentEntities.filter(i => i[PARENT_TYPE_NAME]);
if (!interviews.length) {
    const s = args.currentEntities.length > 1 ? "s" : "";
    throw new Error(`Please connect the ${TYPE_NAME}${s} to a ${PARENT_TYPE_NAME}`);
}

if (!interviews.every(i => i[RICH_TEXT_FIELD_NAME] && i[RICH_TEXT_FIELD_NAME].secret)) {
    throw new Error(`Please add "${RICH_TEXT_FIELD_NAME}" rich-text Field or tweak this button`);
}

const templates = await Promise.all(interviews.map(async (interview) => {
    const study = await fibery.getEntityById(PARENT_TYPE_NAME, interview[PARENT_TYPE_NAME].id, [TEMPLATE_FIELD_NAME]);
    const templateDoc = await fibery.getEntityById("Document", study[TEMPLATE_FIELD_NAME].id, ["secret"]);
    const template = await fibery.getDocumentContent(templateDoc.secret, "html");
    const notes = await fibery.getDocumentContent(interview[RICH_TEXT_FIELD_NAME].secret);

    return {
        id: interview.id,
        notes: { secret: interview[RICH_TEXT_FIELD_NAME].secret, text: notes },
        text: template
    };
}));

const templatesToInsert = templates.filter(t => t.text && t.text.length > 0);

if (!templatesToInsert.length) {
    const errorMessage = templates.length > 1 ? "All templates are emtpy" : "The template is empty";
    throw new Error(errorMessage);
}

await Promise.all(templatesToInsert.map(async (template) => {
    const method = template.notes.text && template.notes.text.length > 0 ? "appendDocumentContent" : "setDocumentContent";
    return await fibery[method](template.notes.secret, template.text, "html");
}));

const successMessage = templatesToInsert.length > 1 ? "The templates are coming..." : "The template is coming...";
return successMessage;