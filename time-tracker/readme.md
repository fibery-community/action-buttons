![time tracker demo](time-tracker.gif)

### Fields
* **[Date]** Timer Started
* **[Number.Decimal]** Latest Time Entry
* **[Number.Decimal]** Time Spent