const api = context.getService("fibery");

const activeEntities = args.currentEntities.filter(e => e["Timer Started"]);
for (const entity of activeEntities) {
    const duration = (new Date().getTime() - new Date(entity["Timer Started"]).getTime()) / (60 * 1000);
    const timeSpent = Number.parseFloat(entity["Time Spent"]) || 0.0;
    await api.updateEntity(entity.type, entity.id, {
        "Timer Started": null,
        "Latest Time Entry": duration,
        "Time Spent": timeSpent + duration
    });
}