const api = context.getService('fibery');
const http = context.getService('http');
const utils = context.getService("utils");

// get Entity.Assignees.SlackMemberId
async function getAssigneesSlackIds(type, id) {
    const entity = await api.getEntityById(type, id, ["Assignees"]);
    return api.getEntitiesByIds('User', entity["Assignees"].map(a => a.id), ["Slack Member ID"]);
}

for (const entity of args.currentEntities) {
    const [assignees, summary] = await Promise.all([
        getAssigneesSlackIds(entity.type, entity.id),
        api.getDocumentContent(entity["Summary"].secret)
    ]);

    const assigneesText = assignees.map(a => `<@${a["Slack Member ID"]}>`).join(', ');
    const url = utils.getEntityUrl(entity.type, entity["Public Id"]);

    await http.postAsync('https://hooks.slack.com/services/YOUR_WEBHOOK', {
        body: {
            text: `💡 <${url}|${entity.name}> by ${assigneesText}\n🎯 ${entity["Strategic Objective"].name}\n"_${summary}_"`
        },
        headers: { 'Content-type': 'application/json' }
    });
}