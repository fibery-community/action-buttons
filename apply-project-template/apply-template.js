// when creating a new Space or Space template, please take the latest version of the script from Gitlab
// https://gitlab.com/fibery-community/action-buttons/-/blob/master/apply-project-template/apply-template.js

const fibery = context.getService('fibery');
const schema = await fibery.getSchema();

/*
Terms 
 */
const TASK_TYPES = ["Task"];
const PROJECT_TEMPLATE_TYPE_NAME = null; // specify if it differs from "PROJECT_TYPE Template" pattern
const TASK_TEMPLATE_TYPE_NAME = null; // specify if it differs from "TASK_TYPE Template" pattern



/*
Configuration
 */
const DO_NOT_COPY_FIELDS = ["References", "Files", "Documents", "Whiteboards"];
const COPY_PROJECT_RICH_TEXT_FIELDS = true;
const PROJECT_PREFIX_FIELD = null; // specify to get "[PREFIX] Task name" for generated tasks



/*
Auxiliary Functions
(here the realm of a solution architect ends, and the realm of a programmer begins)
 */
function findRelation(fromTypeFull, toType) {
    const fields = schema.typeObjects.find(t => t.name === fromTypeFull).fieldObjects;
    const relationField = fields.find(f => f.type.endsWith(`/${toType}`));
    if (!relationField) {
        throw new Error(`Oops! The relation to ${toType} Type is missing.`)
    }
    return relationField;
}


function parseProjectNames(projectTypeFull) {
    const [projectApp, projectType] = projectTypeFull.split('/');

    const projectTemplateType = PROJECT_TEMPLATE_TYPE_NAME || `${projectType} Template`;
    const projectTemplateTypeFull = findRelation(projectTypeFull, projectTemplateType).type;
    const [projectTemplateApp, projectTemplateTypeDuplicate] = projectTemplateTypeFull.split('/');

    return {
        app: projectApp,
        type: projectType,
        typeFull: projectTypeFull,

        templateApp: projectTemplateApp,
        templateType: projectTemplateType,
        templateTypeFull: projectTemplateTypeFull
    }
}

function parseTaskNames(projectNames, taskType) {
    const taskTypeFull = findRelation(projectNames.typeFull, taskType).type;
    const [taskApp, taskTypeDuplicate] = taskTypeFull.split('/');

    const taskTemplateType = TASK_TEMPLATE_TYPE_NAME || `${taskType} Template`;
    const taskTemplatesField = findRelation(projectNames.templateTypeFull, taskTemplateType);

    return {
        app: taskApp,
        type: taskType,
        typeFull: taskTypeFull,

        templateType: taskTemplateType,
        templateTypeFull: taskTemplatesField.type,
        templatesField: taskTemplatesField.title
    }
}

function getTemplateFields(taskTemplateTypeFullName, taskTypeFullName) {
    const taskTemplateFields = schema.typeObjectsByName[taskTemplateTypeFullName].fieldObjects;
    const taskFields = schema.typeObjectsByName[taskTypeFullName].fieldObjects;

    const areFieldsTheSame = (tf, f) => f.title === tf.title && (
        (f.type === tf.type) ||
        (f.typeObject.isEnum && tf.typeObject.isEnum && f.cardinality === tf.cardinality) ||
        (f.type === taskTypeFullName && tf.type === taskTemplateTypeFullName && f.cardinality === tf.cardinality)
    )

    return taskTemplateFields.filter(tf =>
        taskFields.some(f => !f.isReadOnly && areFieldsTheSame(tf, f)) &&
        !DO_NOT_COPY_FIELDS.includes(tf.title));
}

function buildTask(templateFields, templateTask, namePrefix = null) {
    return templateFields.reduce((acc, f) => {
        if (f.isTitle) {
            const prefix = namePrefix ? `[${namePrefix}] ` : '';
            return { ...acc, [f.title]: `${prefix}${templateTask[f.title]}` };
        } else if (f.type === "fibery/Button" || f.type === "Collaboration~Documents/Document" || f.isReadOnly) {
            return acc;
        } else if (f.typeObject.isPrimitive) {
            return { ...acc, [f.title]: templateTask[f.title] };
        } else if (f.typeObject.isEnum && f.cardinality === ":cardinality/many-to-one") {
            // Workaround for issue with setting null to enums
            const enumSetter = templateTask[f.title].Name ? { [f.title]: templateTask[f.title].Name } : {};
            return { ...acc, ...enumSetter };
        } else if (f.typeObject.isPrimitive === false && f.cardinality === ":cardinality/many-to-one") {
            return { ...acc, [f.title]: templateTask[f.title].id };
        } else {
            return acc;
        }
    }, {});
}

async function copyRichText(templateDocumentSecret, targetDocumentSecret, appendIfNotEmpty = false) {
    const templateContent = await fibery.getDocumentContent(templateDocumentSecret, "json");
    if (!templateContent) {
        return;
    }

    if (!appendIfNotEmpty) {
        return await fibery.setDocumentContent(targetDocumentSecret, templateContent, "json");
    } else {
        const currentContent = await fibery.getDocumentContent(targetDocumentSecret, "md");
        const method = currentContent ? "appendDocumentContent" : "setDocumentContent";
        return await fibery[method](targetDocumentSecret, templateContent, "json");
    }
}


/*
The Real Deal
 */
const projectNames = parseProjectNames(args.currentEntities[0].type);

const projects = args.currentEntities.filter(e => e[projectNames.templateType].id);
if (!projects.length) {
    const article = args.currentEntities.length === 1 ? "the" : "each";
    throw new Error(`Please pick a ${projectNames.templateType} for ${article} ${projectNames.type}`);
}

const projectRichTextFields = getTemplateFields(projectNames.templateTypeFull, projectNames.typeFull)
    .filter(tf => tf.type === "Collaboration~Documents/Document");

if (PROJECT_PREFIX_FIELD && projects.some(p => !p.hasOwnProperty(PROJECT_PREFIX_FIELD))) {
    throw new Error(`Project prefix Field ("${PROJECT_PREFIX_FIELD}") is not found.`);
}


const taskTypes = TASK_TYPES.map(tt => {
    const names = parseTaskNames(projectNames, tt);
    const templateFields = getTemplateFields(names.templateTypeFull, names.typeFull);
    return {
        names: names,
        templateFields: templateFields,
        richTextFields: templateFields.filter(tf => tf.type === "Collaboration~Documents/Document"),
        dependencyField: templateFields.find(tf => tf.type === names.templateTypeFull)
    };
});


for (const project of projects) {
    const projectTemplate = await fibery.getEntityById(
        projectNames.templateTypeFull,
        project[projectNames.templateType].id,
        [...taskTypes.map(tt => tt.names.templatesField), ...projectRichTextFields.map(rtf => rtf.title)]
    );

    if (COPY_PROJECT_RICH_TEXT_FIELDS) {
        for (const richTextField of projectRichTextFields) {
            await copyRichText(projectTemplate[richTextField.title].secret, project[richTextField.title].secret, true);
        }
    }

    for (const taskType of taskTypes) {
        const taskTemplateIds = projectTemplate[taskType.names.templatesField].map(tt => tt.id);
        const taskTemplates = await fibery.getEntitiesByIds(taskType.names.templateTypeFull, taskTemplateIds,
            taskType.templateFields.map(tf => tf.title)
        );

        const tasks = await Promise.all(taskTemplates.map(async (taskTemplate) => {
            const task = await fibery.createEntity(taskType.names.typeFull, {
                ...buildTask(taskType.templateFields, taskTemplate, project[PROJECT_PREFIX_FIELD]),
                [projectNames.type]: project.id,
                [taskType.names.templateType]: taskTemplate.id,
                "Created By": args.currentUser.id
            });

            if (taskTemplate["Assignees"] && taskTemplate["Assignees"].length) {
                for (const assignee of taskTemplate["Assignees"]) {
                    await fibery.assignUser(taskType.names.typeFull, task.id, assignee.id);
                }
            }

            for (const richTextField of taskType.richTextFields) {
                await copyRichText(taskTemplate[richTextField.title].secret, task[richTextField.title].secret);
            }

            return task;
        }));

        if (taskType.dependencyField) {
            const templateToTask = taskTemplates.reduce((acc, tt, i) => ({
                ...acc,
                [tt.id]: tasks[i].id
            }), {});

            for (const taskTemplate of taskTemplates) {
                const taskId = templateToTask[taskTemplate.id];
                for (const dependency of taskTemplate[taskType.dependencyField.title]) {
                    const blockerTaskId = templateToTask[dependency.id];
                    await fibery.addCollectionItem(taskType.names.typeFull, taskId, taskType.dependencyField.title, blockerTaskId);
                }
            }
        }
    }
}

return "✅ Done";